#include"Tree.h"
#include<fstream>
#include"date.h"
using namespace std;
Tree::list::list()
{
	parant = 0;
	left = 0;
	right = 0;
	info = 0;
	num = -1;
}
Tree::list::~list()
{
	delete info;
}
bool Tree::list::operator<(list &a)
{
	if (num < a.num)
		return 1;
	else return 0;
}
bool Tree::list::operator==(list &a)
{
	if (num == a.num)
		return 1;
	else return 0;
}
void Tree::list::editInfo(object &date)
{
	if (this->info != 0)
		delete this->info;
	if (date.typeId() == 1)//���� Int
		this->info = new MyInt(*(MyInt*)&date);
	else if (date.typeId() == 2)
		this->info = new MyString(*(MyString*)&date);
	else if (date.typeId() == 3)
		this->info = new class date(*(class date*)&date);
}
void Tree::list::editNumber(int number)
{
	num = number;
}
Tree::list::list(const list &a)
{
	left = a.left;
	right = a.right;
	parant = a.parant;
	if (a.info->typeId() == 1)//���� Int
		info = new MyInt(*(MyInt*)a.info);
	else if (a.info->typeId() == 2)
		info = new MyString(*(MyString*)a.info);
	else if (a.info->typeId() == 3)
		info = new date(*(date*)a.info);
	num = a.num;
}


bool Tree::list::operator>(list &a)
{
	if (num > a.num)
		return 1;
	else return 0;
}


Tree::Tree()
{
	root = 0;
}
Tree::~Tree()
{
	if (!isEmpty())
		recDelete(root);
}

void Tree::recDelete(list *a)
{
	if (a==NULL) 
		return;
	recDelete(a->left);
    recDelete(a->right); 
	delete a;
} 

void Tree::Print(int ob)
{
	if (!isEmpty())
		recPrint(root,ob);
}

void Tree::recPrint(list *a,int ob)
{
	if (a==NULL) 
		return;
	if (ob == 1)
	{
		cout<<a->num<<":"<<a->info->nameType()<<" " ;
		a->info->print();
		cout << " ";
	}
	recPrint(a->left,ob);
	if (ob == 2)
	{
		cout<<a->num<<":"<<a->info->nameType()<<" " ;
		a->info->print();
		cout << " ";
	}
    recPrint(a->right,ob); 
	if (ob == 3)
	{
		cout<<a->num<<":"<<a->info->nameType()<<" " ;
		a->info->print();
		cout << " ";
	}
}

void Tree::Insert(object& date, int number)//������� � �������
{
	if (isEmpty())
	{
		root = new list;
		root->editInfo(date);
		root->editNumber(number);
	}else
	{
		if (Find(number)==0)
			recInsert(&date,number,root);
		else cout <<"������ � ����� ��������������� ��� ����������"<<endl;
	}
}

void Tree::recInsert(object *date, int number,list *a)
{
	if (number<a->num)
	{
		if (a->left != NULL)
			recInsert(date,number,a->left);
		else
		{
			a->left =new list();
			a->left->editInfo(*date);
			a->left->editNumber(number);
			a->left->parant = a;
		}
	}else
	{
		if (a->right != NULL)
			recInsert(date,number,a->right);
		else
		{
			a->right =new list();
			a->right->editInfo(*date);
			a->right->editNumber(number);
			a->right->parant = a;
		}
	}
}
void Tree::Edit(object& date, int number)//��������� � �������
{
	list *a = recFind(number,root);
	a->editInfo(date);
}
void Tree::Delete(int number)
{
	if (isEmpty())
		return;
	list *del = recFind(number,root);
	if (del->right == 0 && del->left == 0)//���� ��� ��������
	{
		if (del->parant != 0)//���� �� ������
		{
			if (del->parant ->right == del)
				del->parant ->right = 0;
			if (del->parant ->left == del)
				del->parant ->left = 0;
		}else
		{
			root = 0;
		}
		delete del;
	}
	else if ((del->right == 0 && del->left != 0) )//���� ���� ���� ������� �����, �� �� ������� �� ����� ����������
	{
		if (del->parant != 0)//���� �� ������
		{
			if (del->parant ->right == del)
				del->parant->right = del->left;
			if (del->parant ->left == del)
				del->parant->left = del->left;
		}
		else
		{
			root = del->left;
		}
		delete del;
	}else if (del->right != 0 && del->left == 0)//���� ���� ���� ������� ������, �� �� ������� �� ����� ����������
	{
		if (del->parant != 0)//���� �� ������
		{
			if (del->parant ->right == del)
				del->parant->right = del->right;
			if (del->parant ->left == del)
				del->parant->left = del->right;
		}else
		{
			root = del->right;
		}
		delete del;
	}else // ���� 2 �������
	{
		if (del->left->right == 0)//���� � ���������� ���� �����, � �������� ��� �������, �� �� ����� ��������
		{
			if (del->parant != 0)//���� �� ������
			{
				if (del->parant ->right == del)
					del ->parant->right = del->left;
				if (del->parant ->left == del)
					del ->parant->left = del->left;
			}else
			{
				root = del->left;
				root->right = del->right;
			}
			delete del;	
			return;
		}
		if (del->right->left == 0)//���� � ���������� ���� ������, � �������� ��� ������, �� �� ����� ��������
		{
			if (del->parant != 0)
			{
				if (del->parant ->right == del)
					del ->parant->right = del->right;
				if (del->parant ->left == del)
					del ->parant->left = del->right;
			}else
			{
				root = del->right;
				root->left = del->left;
			}
			delete del;	
			return;
		}
		//���� ��� ���� ����� ������� �������, �� ����� ����� ��� ������(� ���� �����) � ���� �� ���� �� ����� ������(�� ������) ��� �����(�� �������)
		{
			list *l = del->left;//l - ��������� ������ �� ������ ������� ����������
			while(1)
			{
				if (l->right == 0)
					break;
				l = l->right;
			}
			if (l->left == 0)//���� � ���������� ����� ��� ��������� �����
				l->parant->right = 0;//�� ������ ���������� ��� �� ������
			else//���� ��� ���� � ���� ���� ���������, �� ��������� ��� � ������ ����������
				l->parant->right = l->left;
			l->left = del ->left;//��������� ��� �� ����� ����������
			l->right = del->right;
			if (del->parant != 0)//���� �� ������
			{
				if (del->parant ->right == del)
					del ->parant->right = l;
				if (del->parant ->left == del)
					del ->parant->left = l;
			}else
				root = l;
			delete del;
		}
	}
}
object* Tree::Find(int number)//���������� ������� �� ��������� �� ������
{
	if (isEmpty())
		return 0;
	list *a =recFind(number, root);
	if (a != 0)
		return a->info;
	else return 0;
}
Tree::list* Tree::recFind(int number, list *a)//����������� �������, ���������� ����� � ���������//������� ���������� �� ����� ������, ����� ���������� ��� ��������
{
	if (a->num==number) //���� ������� ����� ����� ����� � ���������
		return a;//������� ������� ���������
	if (a->left!=0)
	{
		list *r = recFind(number,a->left);//���� �� ������, �� ���� ���� �� �����, �� ������� ��� �� ������� ��� ������� �����
		if (r != 0)
			return r;
	}
	if (a->right!=0)
	{
		list *r = recFind(number,a->right); //����� ������� ������� ��� ����� ����������, ��� �������� � ������
		if (r != 0)
		return r;
	}
	return 0;
}

int Tree::count(list *Root)
{
    if (Root == 0)
        return 0;
    return count(Root->right) + count(Root->left) + 1;
}

ofstream& Tree::write(ofstream& os)
{
	if (isEmpty())
		return os;
	int count = this->count(root);
	os.write((char*)&count,sizeof(int));
	recWrite(root, os);
	return os;
}
ofstream& Tree::recWrite(list *Root,ofstream &os)
{
	if (Root == 0)
		return os;
	int ty = Root->info->typeId();
	int number = Root->num;
	os.write((char*)&ty,sizeof(int));
	os.write((char*)&number,sizeof(int));
	Root->info->write(os);
	recWrite(Root->right,os);recWrite(Root->left,os);
	return os;
}

ifstream& Tree::read(ifstream& is)
{
	int count = 0;
	is.read((char*)&count,sizeof(int));
	if (count != 0)
	{
		this->recDelete(root);
		root = 0;
	}else
	{
		return is;
	}
	for (int i=0;i<count;i++)
	{
		int type,number;
		is.read((char*)&type,sizeof(int));
		is.read((char*)&number,sizeof(int)); 
		if (type == 1)
		{
			MyInt a;
			a.read(is);
			this->Insert(a,number);
		}else
		if (type == 2)
		{
			MyString a;
			a.read(is);
			this->Insert(a,number);
		}
		else
		if (type == 3)
		{
			class date a;
			a.read(is);
			this->Insert(a,number);
		}
	}
	return is;
}

void Tree::Summ(int number1,int number2,int number)
{
	object *t1 = Find(number1);
	if (t1 == 0)
	{
		cout << "�� ���������� ������� ��������"<<endl;
		return;
	}
	object *t2 = Find(number2);
	if (t2 == 0)
	{
		cout << "�� ���������� ������� ��������"<<endl;
		return;
	}
	object *t = t1->Add(t2);
	this->Insert(*t,number);
}

bool Tree::isEmpty()
{
	if (root == 0)
		return true;
	return false;
}