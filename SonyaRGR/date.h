#include<iostream>
#include<fstream>
#include"object.h"
using namespace std;
class date:public object
{
private:
	struct
	{
		int year;
		int month;
		int day;
		char *nameofday;
	};
	char* findname(int day,int month,int year);
	int daycount();
	void Checkinfo(int day,int month,int year);
public:
	int ReadyToPrint();
	date(char* _s);
	date(int day,int month,int year,char *str);
	date();
	int length;
	friend void sub(date &a,date &d);
	void add(date &a);
	date(const date &a);
	date&operator=(date &a);
	date&operator=(char *s);
	date  operator+(date &a);
	friend date operator-(date a,date b);
	bool operator<(date &a);
	bool operator>(date &a);
	date&operator++();       
	date operator++(int); 
	operator char*()const;
	operator int();
	~date();
	void edit(int day,int month, int year,char *str);
	void printdate();
	friend ostream&operator<<(ostream &os,date &a);
	friend istream&operator>> (istream& is, date &a) ;
	friend ofstream&operator<< ( ofstream& os, date& a );
	friend ifstream&operator>> ( ifstream& is, date& a ) ;
	void read(ifstream &is);
	void write(ofstream &os);

	int typeId();//	�������� ����������� �������������� ������,
	const char* nameType();//	�������� ��������� �� ������ � ������ ������,
	int cmp(object *obj);//	��������� ���� ��������,
	object* Add(object *obj);//	��������� �(�����������) ���� ��������,
	object* copy();//	�������� ������������ ����� �������.
	void print();
} ;
