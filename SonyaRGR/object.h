#pragma once
#include<iostream>
using namespace std;
class object
{
public:
	virtual ~object();
	virtual void read(ifstream &is);//	���������� ������� � ���������������� �������� ����,
	virtual void write(ofstream &os);	//������ ������� �� ����������������� ��������� �����,
	virtual int cmp(object *obj);//	��������� ���� ��������,
	virtual object* Add(object *obj);//	��������� �(�����������) ���� ��������,
	virtual object* copy();//	�������� ������������ ����� �������.
	virtual void print();
	virtual int typeId();//	�������� ����������� �������������� ������,
	virtual const char* nameType();//	�������� ��������� �� ������ � ������ ������,
	
};

class MyInt:public object
{
private:
	int *i;
public:
	MyInt();
	~MyInt();
	void change(int i);
	MyInt(int i);
	MyInt(const MyInt &i);
	void read(ifstream &is);//	���������� ������� � ���������������� �������� ����,
	void write(ofstream &os);	//������ ������� �� ����������������� ��������� �����,
	int typeId();//	�������� ����������� �������������� ������,
	const char* nameType();//	�������� ��������� �� ������ � ������ ������,
	int cmp(object *obj);//	��������� ���� ��������,
	object* Add(object *obj);//	��������� �(�����������) ���� ��������,
	object* copy();//	�������� ������������ ����� �������.
	void print();
};

class MyString:public object
{
private:
	string *s;
public:
	MyString();
	~MyString();
	MyString(string i);
	void change(string i);
	MyString(const MyString& i);
	int typeId();//	�������� ����������� �������������� ������,
	const char* nameType();//	�������� ��������� �� ������ � ������ ������,
	int cmp(object *obj);//	��������� ���� ��������,
	object* Add(object *obj);//	��������� �(�����������) ���� ��������,
	object* copy();//	�������� ������������ ����� �������.
	void print();
	void read(ifstream &is);//	���������� ������� � ���������������� �������� ����,
	void write(ofstream &os);	//������ ������� �� ����������������� ��������� �����,
	
};
