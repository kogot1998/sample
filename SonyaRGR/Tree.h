#include<iostream>
#include"object.h"

using namespace std;

class Tree
{
private:
	class list
	{
	private:
		object *info;
		int num;//��������� �����, �� �������� ����� ����������� � ������
		list *left;
		list *right;
		list *parant;
	public:
		list();
		~list();
		list(const list &a);
		void editInfo(object &date);
		void editNumber(int number);
		bool operator>(list &a);
		bool operator<(list &a);
		bool operator==(list &a);
		friend Tree;
	} ;
public:
	Tree();
	~Tree();
	void Insert(object& date, int number);//������� � �������
	void Edit(object& date, int number);//��������� � �������
	void Delete(int number);
	void Print(int ob);
	bool isEmpty();
	object* Find(int number);//���������� ������� �� ��������� �� ������
	ofstream& write(ofstream& os);
	ifstream& read(ifstream& is);
	int count(list *Root);
	void Summ(int number1,int number2,int number);
private:
	list *root;
	void recDelete(list *a);
	ofstream& recWrite(list *Root,ofstream &os);
	list* recFind(int number, list *a);
	void recPrint(list *a,int ob);
	void recInsert(object *date, int number, list *a);
};